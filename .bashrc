# General

# Set default editor
export EDITOR=vim

# Prevent history duplicates
export HISTCONTROL=ignoredups

# Add alias file
if [ -f ~/.bash_aliases ]; then
	source ~/.bash_aliases
fi

export WORKON_HOME=~/virtualenvs
