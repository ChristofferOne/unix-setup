syntax on

set number
set ruler
set encoding=utf-8
set tabstop=4
set shiftwidth=4
