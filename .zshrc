setopt histignoredups

export PATH=~/.composer/vendor/bin:$PATH

# Setting PATH for Python 3.5
# The original version is saved in .bash_profile.pysave
PATH="/Library/Frameworks/Python.framework/Versions/3.5/bin:${PATH}"
export PATH
export PATH="/usr/local/opt/tcl-tk/bin:$PATH"

export EDITOR=vim

# Prevent history duplicates
export HISTCONTROL=ignoredups

export WORKON_HOME=~/virtualenvs

alias vi='vim'
alias fuck='echo perl -e '"'"'print pack "H*","4b617474756e67617230343f"'"'"' | sudo -S $(history -p \!\!)'
alias ..='cd ..'
alias c='clear'

alias chrome='open -n -a /Applications/Google\ Chrome.app/Contents/MacOS/Google\ Chrome --args --user-data-dir="/tmp/chrome_dev_test" --disable-web-security'

export PATH
export PATH="/usr/local/opt/tcl-tk/bin:$PATH"

# Kubernetes custom aliases and commands

function ns { export NAMESPACE=$1 ;}

function prod {
	context=$(kubectl config get-contexts | awk '{print $3}' |grep cluster-prod)

	res=$(kubectl config use-context $context)
	echo $res
}

function test {
	context=$(kubectl config get-contexts | awk '{print $3}' |grep cluster-test)
    
    res=$(kubectl config use-context $context)
    echo $res
}

alias ks='kubectl --namespace=$NAMESPACE'

function devup {
	docker start 4b67b1e19ade b778de006962 d436c125b858
}

function devdown {
	docker stop 4b67b1e19ade b778de006962 d436c125b858
}


